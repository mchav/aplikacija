﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Configuration;

namespace Aplikacija.Models
{
    public class PostModel
    {
            
            public string PostID { get; set; }
            
            [Required]
            [Display(Name = "Наслов:")]
            [ScaffoldColumn(false)]
            public string Title { get; set; }

            [Required]
            [Display(Name = "Содржина:")]
            public string Sodrzina { get; set; }

            public string UserID { get; set; }
            public string PostAuthor { get; set; }
            public string Photo { get; set; }
     }

        public class Posts
        {
            public static Users authors = new Users();
            public Posts()
            {
                List<PostModel> _pom = GetAllPosts();
                for (int i = 0; i < _pom.Count; i++)
                {
                    _postList.Add(new PostModel
                    {
                        Title = _pom[i].Title,
                        Sodrzina = _pom[i].Sodrzina,
                        UserID = _pom[i].UserID,
                        PostID = _pom[i].PostID,
                        Photo = _pom[i].Photo,
                        PostAuthor = authors.GetUserNameByID(_pom[i].UserID)
                    });
                }
            }

            public List<PostModel> GetAllPosts()
            {
                List<PostModel> us = new List<PostModel>();
                //postavuvanje na vrskata
                SqlConnection kon = new SqlConnection();
                kon.ConnectionString = ConfigurationManager.ConnectionStrings["MojaKonekcija"].ConnectionString;

                SqlCommand post = new SqlCommand();
                post.Connection = kon;
                post.CommandText = "SELECT Title, Sodrzina, PostID, UserID , Slika FROM Post";

                SqlDataAdapter adapret = new SqlDataAdapter();
                adapret.SelectCommand = post;
                try
                {
                    kon.Open();
                    SqlDataReader citac = post.ExecuteReader();
                     
                    while (citac.Read())
                    {
                        PostModel poss = new PostModel();
                        poss.Title = Convert.ToString(citac[0]);
                        poss.Sodrzina = Convert.ToString(citac[1]);
                        poss.PostID = Convert.ToString(citac[2]);
                        poss.UserID = Convert.ToString(citac[3]);
                        poss.Photo = Convert.ToString(citac[4]);
                        us.Add(poss);
                    }
                    citac.Close();

                }
                catch (Exception exc)
                {
                }
                finally
                {
                    kon.Close();
                }
                return us;
            }

            public List<PostModel> _postList = new List<PostModel>();


            public void Create(PostModel umToUpdate)
            {
                //postavuvanje na vrskata
                SqlConnection kon = new SqlConnection();
                SqlCommand user = new SqlCommand();
                user.Connection = kon;

                kon.ConnectionString = ConfigurationManager.ConnectionStrings["MojaKonekcija"].ConnectionString;
                user.CommandText = "INSERT INTO Post (Title, Sodrzina, UserID) VALUES (@SB, @Kol, @Kat)";
                user.Parameters.AddWithValue("@SB", umToUpdate.Title);
                user.Parameters.AddWithValue("@Kol", umToUpdate.Sodrzina);
                user.Parameters.AddWithValue("@Kat", umToUpdate.UserID);

                try
                {
                    kon.Open();
                    user.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                }
                finally
                {
                    kon.Close();
                }

            }

            public void Remove(string usrName)
            {
                _postList = GetAllPosts();
                foreach (PostModel um in _postList)
                {
                    if (um.Title == usrName)
                    {
                        _postList.Remove(um);
                        break;
                    }
                }
            }

            public PostModel GetPost(string uid)
            {
                _postList = GetAllPosts();
                PostModel usrMdl = null;

                foreach (PostModel um in _postList)
                    if (um.Title == uid)
                        usrMdl = um;

                return usrMdl;
            }

     }    
}