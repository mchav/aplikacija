﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Aplikacija.Models
{
    public class UserModel
    {
        [Required]
        [Display(Name = "Корисничко име:")]
        [ScaffoldColumn(false)]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Е-mail:")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Лозинка:")]
        public string Password { get; set; }

        public string UserID { get; set; }
        
    }

    public class Users
    {

        public Users()
        {
            List<UserModel> _pom = GetAllUsers();
            for (int i = 0; i < _pom.Count; i++)
            {
                _usrList.Add(new UserModel
                {
                    UserName = _pom[i].UserName,
                    Email = _pom[i].Email,
                    Password = _pom[i].Password,
                    UserID = _pom[i].UserID
                });
            }
        }

        public List<UserModel> GetAllUsers()
        {
            List<UserModel> us = new List<UserModel>();
            //postavuvanje na vrskata
            SqlConnection kon = new SqlConnection();
            kon.ConnectionString = ConfigurationManager.ConnectionStrings["MojaKonekcija"].ConnectionString;

            SqlCommand users = new SqlCommand();
            users.Connection = kon;
            users.CommandText = "SELECT UserName, Email, Password, UserID FROM Users";

            SqlDataAdapter adapret = new SqlDataAdapter();
            adapret.SelectCommand = users;
            try
            {
                kon.Open();
                SqlDataReader citac = users.ExecuteReader();
                while (citac.Read())
                {
                    UserModel user = new UserModel();
                    user.UserName = Convert.ToString(citac[0]);
                    user.Email = Convert.ToString(citac[1]);
                    user.Password = Convert.ToString(citac[2]);
                    user.UserID = Convert.ToString(citac[3]);
                    us.Add(user);               
                }
                citac.Close();
                
            }
            catch (Exception exc)
            {            
            }
            finally
            {
                kon.Close();
            }
            return us;
        }

        public List<UserModel> _usrList = new List<UserModel>();


        public string GetUserNameByID(string id)
        {
            string pom = null;
            _usrList = GetAllUsers();
            foreach (var u in _usrList)
            {
                if (u.UserID == id)
                {
                    pom = u.UserName;
                }
            }
            return pom;
        }

        public void Create(UserModel umToUpdate)
        {
            foreach (UserModel um in _usrList)
            {
                if (um.UserName == umToUpdate.UserName)
                {
                    throw new System.InvalidOperationException("Duplicat username: " + um.UserName);
                }
            }
            //postavuvanje na vrskata
            SqlConnection kon = new SqlConnection();
            SqlCommand user = new SqlCommand();
            user.Connection = kon;

            kon.ConnectionString = ConfigurationManager.ConnectionStrings["MojaKonekcija"].ConnectionString;
            user.CommandText = "INSERT INTO Users (UserName, Email, Password) VALUES (@SB, @Kol, @Kat)";
            user.Parameters.AddWithValue("@SB", umToUpdate.UserName);
            user.Parameters.AddWithValue("@Kol", umToUpdate.Email);
            user.Parameters.AddWithValue("@Kat", umToUpdate.Password);
            
            try
            {
                kon.Open();
                user.ExecuteNonQuery();
            }
            catch (Exception exc)
            {
            }
            finally
            {
                kon.Close();
            }
            
        }

        public void Remove(string usrName)
        {
            _usrList = GetAllUsers();
            foreach (UserModel um in _usrList)
            {
                if (um.UserName == usrName)
                {
                    _usrList.Remove(um);
                    break;
                }
            }
        }

        public UserModel GetUser(string uid)
        {
            _usrList = GetAllUsers();
            UserModel usrMdl = null;

            foreach (UserModel um in _usrList)
                if (um.UserName == uid)
                    usrMdl = um;

            return usrMdl;
        }

    }    

}