﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aplikacija.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Drawing;

namespace Aplikacija.Controllers
{

    
    public class HomeController : Controller
    {
        public static Users _usrs = new Users();
        public static Posts _posts = new Posts(); 
        //
        // GET: /Home/
        

        public ActionResult Index()
        {
            Posts posts = new Posts();
            var pom = posts._postList;
            return View(pom); 
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpGet]
        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(UserModel m)
        {
            var pom = _posts;
            for (int i = 0; i < _usrs._usrList.Count; i++)
            {
                if (_usrs._usrList[i].UserName == m.UserName && _usrs._usrList[i].Password == m.Password)
                {
                    Session["user"] = _usrs._usrList[i].UserName;
                    Session["id"] = _usrs._usrList[i].UserID;
                }
               
            }

            return RedirectToAction("Index", "Home");


        }

        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(UserModel model)
        {

            //postavuvanje na vrskata
            SqlConnection kon = new SqlConnection();
            kon.ConnectionString = ConfigurationManager.ConnectionStrings["MojaKonekcija"].ConnectionString;
            SqlCommand komanda = new SqlCommand();
            komanda.Connection = kon;
            komanda.CommandText = "INSERT INTO Users (UserName, Email, Password) VALUES (@UserName, @Mail, @Lozinka)";
            komanda.Parameters.AddWithValue("@UserName", model.UserName);
            komanda.Parameters.AddWithValue("@Mail", model.Email);
            komanda.Parameters.AddWithValue("@Lozinka", model.Password);
            
            try
            {
                kon.Open();
                komanda.ExecuteNonQuery();
                Session["user"] = model.UserName;
                Session["id"] = model.UserID;
            }
            catch (Exception exc)
            {

                Response.Write(exc.Message);
            }
            finally
            {
                kon.Close();
            }



            return RedirectToAction("Index", "Home");
        }

        public ActionResult CreateNewPost()
        {
            return View();
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult CreateNewPost(PostModel model, HttpPostedFileBase File)
        {


            //postavuvanje na vrskata
            SqlConnection kon = new SqlConnection();
            kon.ConnectionString = ConfigurationManager.ConnectionStrings["MojaKonekcija"].ConnectionString;
            SqlCommand komanda = new SqlCommand();

            //Se inicijaliziraat pocetnite promenlivi
            string sSavePath;
            string sThumbExtension;
            int intThumbWidth;
            int intThumbHeight;

            //Se postavuvaat nekoi konstantni vrednosti
            sSavePath = "..\\Pictures\\"; //pateka do folderot vo koj ke se zacuvuvaat logo-ata
            sThumbExtension = "_thumb";
            intThumbWidth = 160;
            intThumbHeight = 120;

            if (File != null)
            {
                //Proverka na goleminata na logoto
                HttpPostedFileBase myFile = File;
                int nFileLen = myFile.ContentLength;

                //Logoto se vcituva vo tek od bajti
                byte[] myData = new Byte[nFileLen];
                myFile.InputStream.Read(myData, 0, nFileLen);

                //Proverka za toa dali veke postoi imeto na logo. Dokolku postoi, se dodava 
                //inkrementalna numericka vrednost dodeka ne se dobie unikatno ime
                string sFilename = System.IO.Path.GetFileName(myFile.FileName);
                int file_append = 0;
                while (System.IO.File.Exists(Server.MapPath(sSavePath + sFilename)))
                {
                    file_append++;
                    sFilename = System.IO.Path.GetFileNameWithoutExtension(myFile.FileName)
                                     + file_append.ToString() + ".jpg";
                }

                //Snimanje na tekot od bajti na disk
                System.IO.FileStream newFile
                        = new System.IO.FileStream(Server.MapPath(sSavePath + sFilename),
                                                   System.IO.FileMode.Create);
                newFile.Write(myData, 0, myData.Length);
                newFile.Close();

                //Proverka na JPG ekstenzijata na logoto, preku negovo otvoranje
                System.Drawing.Image.GetThumbnailImageAbort myCallBack =
                               new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
                Bitmap myBitmap;

                myBitmap = new Bitmap(Server.MapPath(sSavePath + sFilename));

                //dokolku e jpg, kreiraj thumbnail so edinstveno ime
                file_append = 0;
                string sThumbFile = System.IO.Path.GetFileNameWithoutExtension(myFile.FileName)
                                                            + sThumbExtension + ".jpg";
                while (System.IO.File.Exists(Server.MapPath(sSavePath + sThumbFile)))
                {
                    file_append++;
                    sThumbFile = System.IO.Path.GetFileNameWithoutExtension(myFile.FileName) +
                                    file_append.ToString() + sThumbExtension + ".jpg";
                }

                // Save thumbnail
                System.Drawing.Image myThumbnail = myBitmap.GetThumbnailImage(intThumbWidth, intThumbHeight, myCallBack, IntPtr.Zero);
                myThumbnail.Save(Server.MapPath(sSavePath + sThumbFile));
                //    imgPicture.ImageUrl = sSavePath + sThumbFile;
                string s = "~\\" + sSavePath + sThumbFile; //Response.Write(s);
                var m = model.Sodrzina;                
                komanda.Connection = kon;
                komanda.CommandText = "INSERT INTO Post (Title, Sodrzina, UserID, Slika) VALUES (@Title, @Sodrzina, @UserID, @slika)";
                komanda.Parameters.AddWithValue("@Title", model.Title);
                komanda.Parameters.AddWithValue("@Sodrzina", m);
                komanda.Parameters.AddWithValue("@UserID", Session["id"]);
                komanda.Parameters.AddWithValue("@slika", s);
                myThumbnail.Dispose();
                myBitmap.Dispose();

            }
            else
            {
                var m = model.Sodrzina;
                komanda.Connection = kon;
                komanda.CommandText = "INSERT INTO Post (Title, Sodrzina, UserID) VALUES (@Title, @Sodrzina, @UserID)";               
                komanda.Parameters.AddWithValue("@Title", model.Title);
                komanda.Parameters.AddWithValue("@Sodrzina", m);
                komanda.Parameters.AddWithValue("@UserID", Session["id"]);
                //komanda.Parameters.AddWithValue("@slika", s);
            }
                try
                {
                    kon.Open();
                    komanda.ExecuteNonQuery();
                }
                catch (Exception exc)
                {

                    Response.Write(exc.Message);
                }
                finally
                {
                    kon.Close();
                }

                return RedirectToAction("Index", "Home");
            
        }

        public ActionResult LogOff()
        {
            Session["user"] = null;

            return RedirectToAction("Index", "Home");
        }

        protected bool ThumbnailCallback()
        {
            return false;
        }//END
    }
}
